from mayavi import mlab
import numpy as np
import sklearn.cluster as cls

def calculate_db_scan_parameters(pcd):
    # optimised coefficients for the minimum samples calculations
    a = 0.0000711
    b = 40.45
    nb_pts = pcd.shape[0]
    # pcd = pcd[np.where(pcd[:, 2] > -0.2)]
    length = np.max(pcd[:, 0])
    ratio = nb_pts / length

    # best epsilon tested
    epsilon = 0.01
    minimum_samples = a * ratio + b
    return epsilon, minimum_samples


def remove_outliers(pcd, nb_spheres, eps, min_samples):
    # Apply a DBSCAN clustering algorithm
    test_dbscan = cls.DBSCAN(eps=eps, min_samples=min_samples).fit(pcd)

    # Get rid of outliers
    x_to_delete = np.where(test_dbscan.labels_ == -1)
    test_dbscan.labels_ = np.delete(test_dbscan.labels_, x_to_delete)
    pcd = np.delete(pcd, x_to_delete, 0)
    x = []

    for i in range(np.max(test_dbscan.labels_) + 1):
        x.append(np.where(test_dbscan.labels_ == i)[0].shape[0])
    x = np.array(x)
    index_sorted_x = np.argsort(x)

    for i in index_sorted_x[:-nb_spheres]:
        x_to_delete = np.where(test_dbscan.labels_ == i)
        test_dbscan.labels_ = np.delete(test_dbscan.labels_, x_to_delete, 0)
        pcd = np.delete(pcd, x_to_delete, 0)
    return pcd


def isolate_spheres(pcd, nb_spheres):
    """Removes points that do not belong to the spheres.

    Args:
        pcd (NDArray): List of points.
        nb_spheres (int): Number of spheres in the acquisition.

    Returns:
        NDArray: List of points.
    """
    
    # Calculate DBSCAN parameters
    eps, min_samples = calculate_db_scan_parameters(pcd)

    sphere_center = remove_outliers(pcd, nb_spheres, eps, min_samples)

    return sphere_center


def remove_distant_points(pcd):
    """Removes the points outside of the microplot.

    Args:
        pcd (NDArray): List of points.

    Returns:
        NDArray: List of points.
    """
    return pcd[np.where(np.logical_and(pcd[:, 2] < -0.510, pcd[:, 1] < 3.1))]

def transform(pointcloud, matrix):
    """Applies a transformation matrix to a pointcloud

    Args:
        pointcloud (NDArray): List of points.
        matrix (NDArray): Transformation matrix.

    Returns:
        NDArray: List of points.
    """

    coor_rot_imu = np.vstack((pointcloud[0],pointcloud[1],pointcloud[2],np.ones(len(pointcloud[0]))))
    mat_pos_rot = np.column_stack([matrix[:, 0:3], [0, 0, 0, 1]])
    coor_rot_pos = mat_pos_rot.dot(coor_rot_imu)

    pointcloud[0] = coor_rot_pos[0,:]+matrix[0,3] 
    pointcloud[1] = coor_rot_pos[1,:]+matrix[1,3] 
    pointcloud[2] = coor_rot_pos[2,:]+matrix[2,3]

    return pointcloud


def view(pointclouds, downsample=1, mode="point"):
    """View a list of pointclouds with mayavi"""

    colors = [
        (1.0, 0, 0),
        (0, 1.0, 0),
        (0, 0, 1.0),
        (1.0, 1.0, 0),
        (0, 1.0, 1.0),
        (1.0, 0, 1.0),
        (0.5, 0.5, 1.0),
        (1.0, 0.5, 0.5),
        (0.5, 1.0, 0.5),
        (0, 0.5, 1.0),
        (1.0, 0, 0.5),
        (0.5, 1.0, 0),
        (0.5, 0, 1.0),
        (1.0, 0.5, 0),
        (0, 1.0, 0.5),
    ]

    for i, pointcloud in enumerate(pointclouds):
        mlab.points3d(
            pointcloud[0][::downsample],
            pointcloud[1][::downsample],
            pointcloud[2][::downsample],
            mode=mode,
            color=colors[i],
        )

    mlab.show()