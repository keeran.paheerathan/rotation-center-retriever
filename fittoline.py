import numpy as np
import matplotlib.pyplot as plt
import mpl_toolkits.mplot3d as m3d

def fit_to_line(pointcloud):
    x = pointcloud[:,0]
    y = pointcloud[:,1]
    z = pointcloud[:,2]

    data = np.concatenate((x[:, np.newaxis], 
                        y[:, np.newaxis], 
                        z[:, np.newaxis]), 
                        axis=1)

    # Calculate the mean of the points, i.e. the 'center' of the cloud
    datamean = data.mean(axis=0)

    # Do an SVD on the mean-centered data.
    _, _, vv = np.linalg.svd(data - datamean)

    # Now vv[0] contains the first principal component, i.e. the direction
    # vector of the 'best fit' line in the least squares sense.

    # Now generate some points along this best fit line, for plotting.

    # I use -7, 7 since the spread of the data is roughly 14
    # and we want it to have mean 0 (like the points we did
    # the svd on). Also, it's a straight line, so we only need 2 points.
    linepts = vv[0] * np.mgrid[-7:7:2j][:, np.newaxis]

    # shift by the mean to get the line in the right place
    linepts += datamean

    # Verify that everything looks right.

    # ax = m3d.Axes3D(plt.figure())
    # ax.scatter3D(*data.T)
    # ax.plot3D(*linepts.T)
    plt.show()

    return linepts


def distance_to_line(point, line_pointA, line_pointB):
    return np.linalg.norm(np.cross(line_pointB-line_pointA, line_pointA-point))/np.linalg.norm(line_pointB-line_pointA)

def reject_outliers(data, m=2):
    return data[abs(data - np.mean(data)) < m * np.std(data)]