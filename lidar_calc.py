import numpy as np
import utils
import math
import matplotlib.pyplot as plt

from scipy.optimize import fmin


class LidarCalc:
    @staticmethod
    def get_lidar_xy(
        x_raw_ref: np.ndarray,
        y_raw_ref: np.ndarray,
        x_raw: np.ndarray,
        y_raw: np.ndarray,
    ):
        """Gets lidar X and Y coordinates after rotation to align x-axis on the path of acquisition of phenomobile"""
        # The function below compute X-distance between first and last position after rotation alpha
        rot_func = (
            lambda alpha: utils.cos_d(alpha) * x_raw_ref[0]
            - utils.sin_d(alpha) * y_raw_ref[0]
            + utils.cos_d(alpha) * x_raw_ref[-1]
            - utils.sin_d(alpha) * y_raw_ref[-1]
        )

        # The minimum of rot_func is the rotation resulting in an alignment of computed X-axis with path, but in the
        # opposite direction of the direction of acquisition
        #  A -180 rotation orients computed X-axis with the direction of acquisition
        r_opt = fmin(func=rot_func, x0=[0], disp=False) - 180

        rot_matrix = np.array(
            [
                [utils.cos_d(r_opt), -utils.sin_d(r_opt)],
                [utils.sin_d(r_opt), utils.cos_d(r_opt)],
            ]
        )

        m_in = [x_raw, y_raw]
        return rot_matrix.dot(m_in)

    @staticmethod
    def compute_lidar_positions(
        lidar_ref,
        translation_parameters,
        lidar,
        geo,
        measuring_head_180,
        lidar3=False,
        isLidar3Reversed=False,
        mat_transf=np.identity(4),
    ) -> dict:
        # INTERPOLATE GEO LOCALISATION TO LIDAR TIME
        lidar["longitude"] = np.interp(lidar["date"], geo["date"], geo["longitude"])
        lidar["latitude"] = np.interp(lidar["date"], geo["date"], geo["latitude"])
        lidar["tray_height"] = np.interp(lidar["date"], geo["date"], geo["tray_height"])
        lidar["heading"] = np.interp(lidar["date"], geo["date"], geo["heading"])
        lidar["course"] = np.interp(lidar["date"], geo["date"], geo["course"])
        lidar["roll"] = np.interp(lidar["date"], geo["date"], geo["roll"])
        lidar["pitch"] = np.interp(lidar["date"], geo["date"], geo["pitch"])
        lidar["sog"] = np.interp(lidar["date"], geo["date"], geo["sog"])

        if not lidar3:
            lidar_ref["longitude"] = np.interp(
                lidar_ref["date"], geo["date"], geo["longitude"]
            )
            lidar_ref["latitude"] = np.interp(
                lidar_ref["date"], geo["date"], geo["latitude"]
            )
            lidar_ref["tray_height"] = np.interp(
                lidar_ref["date"], geo["date"], geo["tray_height"]
            )
            lidar_ref["heading"] = np.interp(
                lidar_ref["date"], geo["date"], geo["heading"]
            )
            lidar_ref["course"] = np.interp(
                lidar_ref["date"], geo["date"], geo["course"]
            )
            lidar_ref["roll"] = np.interp(lidar_ref["date"], geo["date"], geo["roll"])
            lidar_ref["pitch"] = np.interp(lidar_ref["date"], geo["date"], geo["pitch"])
            lidar_ref["sog"] = np.interp(lidar_ref["date"], geo["date"], geo["sog"])

        # CONVERSION FROM WSG84 TO LAMBERT-93 REFERENTIAL
        (x0, y0) = utils.wsg84_to_lambert93(
            lidar_ref["longitude"][0], lidar_ref["latitude"][0]
        )
        (x, y) = utils.wsg84_to_lambert93(lidar["longitude"], lidar["latitude"])

        lidar["x_raw"] = x0 - x
        lidar["y_raw"] = y0 - y

        if not lidar3:
            lidar_ref["x_raw"] = x0 - x
            lidar_ref["y_raw"] = y0 - y

        # ROTATION TO ALIGN X-AXIS ON THE PHENOMOBILE ACQUISITION PATH
        m_out = LidarCalc.get_lidar_xy(
            lidar_ref["x_raw"], lidar_ref["y_raw"], lidar["x_raw"], lidar["y_raw"]
        )
        lidar["x"] = m_out[0]
        lidar["y"] = m_out[1]

        t_0 = lidar["tray_height"][0]

        lidar["z"] = lidar["tray_height"] - t_0

        # COMPUTE RELATIVE POSITIONS TO FIRST POINT OF ACQUISITION

        x_rot_lidar = lidar["distance"] * 0
        y_rot_lidar = lidar["distance"] * np.sin(lidar["angle"])
        if not lidar3 or isLidar3Reversed:
            y_rot_lidar = -y_rot_lidar
        z_rot_lidar = -lidar["distance"] * np.cos(lidar["angle"])
        coor_rot_imu = np.vstack(
            (x_rot_lidar, y_rot_lidar, z_rot_lidar, np.ones(len(x_rot_lidar)))
        )
        mat_pos_rot = np.column_stack([mat_transf[:, 0:3], [0, 0, 0, 1]])

        #### Translation of the current lidar pointcloud into the reference lidar system of coordinates ####
        coor_rot_pos = mat_pos_rot.dot(coor_rot_imu)

        pt_x = coor_rot_pos[0, :] + mat_transf[0, 3]
        pt_y = coor_rot_pos[1, :] + mat_transf[1, 3]
        pt_z = coor_rot_pos[2, :] + mat_transf[2, 3]

        #### Translate the pointcloud to the center of the measuring head ####
        DX = (
            -0.284
        )  # Translation from the ref lidar to the center of the measuring head in X
        DY = (
            -0.25
        )  # Translation from the ref lidar to the center of the measuring head in Y

        pt_x += DX
        pt_y += DY

        if measuring_head_180:
            pt_x = -pt_x
            pt_y = -pt_y

        # If imu_correction pitch and roll correction will be applied to the pointcloud
        # The way to apply these correction is not definitive
        pt_x += translation_parameters[0]
        pt_y += translation_parameters[1]
        pt_z += translation_parameters[2]

        if not measuring_head_180:
            roll_angle = lidar["roll"] * math.pi / 180
            pitch_angle = lidar["pitch"] * math.pi / 180
        else:
            roll_angle = -lidar["roll"] * math.pi / 180
            pitch_angle = -lidar["pitch"] * math.pi / 180

        """ Pitch and roll correction """
        x = (
            pt_x * np.cos(pitch_angle)
            + pt_y * np.sin(pitch_angle) * np.sin(roll_angle)
            + pt_z * np.sin(pitch_angle) * np.cos(roll_angle)
        )
        y = pt_y * np.cos(roll_angle) - pt_z * np.sin(roll_angle)
        z = (
            -pt_x * np.sin(pitch_angle)
            + pt_y * np.cos(pitch_angle) * np.sin(roll_angle)
            + pt_z * np.cos(pitch_angle) * np.cos(roll_angle)
        )

        x += lidar["x"]
        y += lidar["y"]
        z += lidar["z"]

        return {
            "pt_x": x,
            "pt_y": y,
            "pt_z": z,
        }
