import argparse
import math
import os
import time
import warnings

import matplotlib.pyplot as plt
import numpy as np
import open3d as o3d
import pyransac3d as pyrsc
import skg
from h5_info import Geo, H5Info, constants
from mayavi import mlab
from scipy import optimize as optim
from sklearn.metrics import mean_squared_error

from fittoline import distance_to_line, fit_to_line
from lidar_calc import LidarCalc
from pointcloudhandler import view
from preprocess import preprocess_pointcloud

warnings.filterwarnings("ignore", category=DeprecationWarning)

iteration = 1


def extract(h5_path):
    print("Extracting")
    sensor_names = ["Lidar"]

    h5_info = H5Info()
    h5_info.load_data(h5_path, sensor_names)
    lidars = {}
    for sensor in h5_info.sensors:
        if sensor.type == constants.TYPE_LIDAR:
            image = sensor.images[0]
            lidars[sensor.description] = image.scans

    return [lidars["lms_1"], h5_info]


def cost_funct(translation_parameters, *args):
    start_time = time.time()
    input = args[0]

    lidar_data = input["lidar_data"]
    h5_info = input["h5_info"]
    plane_mask = input["plane_mask"]
    noise_mask = input["noise_mask"]
    measuring_head_180 = input["measuring_head_180"]
    reversed = input["reversed"]

    pointcloud = LidarCalc.compute_lidar_positions(
        lidar_data,
        [
            translation_parameters[0],
            translation_parameters[1],
            translation_parameters[2],
        ],
        lidar_data,
        Geo.to_dict_array(h5_info.geo),
        measuring_head_180,
        lidar3=False,
        isLidar3Reversed=reversed,
    )

    pointcloud = np.array(
        [pointcloud["pt_x"], pointcloud["pt_y"], pointcloud["pt_z"]]
    ).T

    # If there is an error when fitting a circle we redo all the steps
    while True:
        processed_pointcloud = preprocess_pointcloud(pointcloud, plane_mask, noise_mask)

        """ Cut the tube into multiple slices """
        slices = []
        min_x = np.min(processed_pointcloud[:, 0])
        max_x = np.max(processed_pointcloud[:, 0])

        step = 0.005  # Slice thickness

        for x in np.arange(min_x + step, max_x, step):
            slice = processed_pointcloud[
                np.where(
                    np.logical_and(
                        processed_pointcloud[:, 0] < x,
                        processed_pointcloud[:, 0] > x - step,
                    )
                )
            ]

            slices.append(slice)

        centers = []

        """ Fit circles for each slices and save their center """
        for slice in slices:
            try:
                mean_x = np.mean(slice[:, 0])
                _, center = skg.nsphere_fit(slice[:, [1, 2]])
            except RuntimeWarning:
                continue
            except IndexError:
                continue
            except ValueError:
                continue
            center = np.asarray(center)
            center = np.insert(center, 0, mean_x)
            if np.isrealobj(center):
                centers.append(center)

        centers = np.array(centers)

        """ Compute the cost """
        try:
            # Fit a line to all the center points
            line_pointA, line_pointB = fit_to_line(centers)

            y = []

            # Calculate the distance between each center points to the line
            for center in centers:
                dist = distance_to_line(center, line_pointA, line_pointB)

                if dist < 0.05:
                    y.append(dist)

            preds = np.zeros(len(y))

            # The cost is the RMSE between the line and the center points
            rmse = mean_squared_error(y, preds, squared=False)
        except np.ComplexWarning as e:
            print(e)
            rmse = math.inf
            break
        else:
            break

    global iteration

    print(
        "rmse: "
        + str(rmse)
        + ", iteration: "
        + str(iteration)
        + ", time: "
        + str(time.time() - start_time)
    )
    print(
        [
            translation_parameters[0],
            translation_parameters[1],
            translation_parameters[2],
        ]
    )
    iteration += 1
    return rmse


""" Load parser """

parser = argparse.ArgumentParser()
parser.add_argument("--h5_path", required=True) #Path to the tube HDF5 file
parser.add_argument("--output") #Output folder
# Boolean arguments
parser.add_argument("--reversed", action="store_true") #If the lidar 3 is reversed
parser.add_argument("--no_reversed", dest="reversed", action="store_false")
parser.add_argument("--measuring_head_180", action="store_true")#If the measuring head was rotated by 180°
parser.add_argument(
    "--no_measuring_head_180", dest="measuring_head_180", action="store_false"
)
parser.set_defaults(measuring_head_180=False)
parser.set_defaults(reversed=False)

args = parser.parse_args()

print("HDF5 Path: ", args.h5_path)
print("Output folder path Path: ", args.output)
print("Is Lidar 3 reversed: ", args.reversed)
print("Is the measuring head rotated by 180°: ", args.measuring_head_180)

# Create output folder
if not os.path.exists(args.output):
    os.makedirs(args.output)

""" Extract one pointcloud from the HDF5 file """
path_to_h5 = args.h5_path

initParameters = [
    0,
    0,
    -2.4,
]  # Translation [dx, dy, dz] from the center of the measuring head to the center of rotation

lidar_data, h5_info = extract(path_to_h5)

pointcloud = LidarCalc.compute_lidar_positions(
    lidar_data,
    initParameters,
    lidar_data,
    Geo.to_dict_array(h5_info.geo),
    False,
    args.reversed,
)

pointcloud = np.array([pointcloud["pt_x"], pointcloud["pt_y"], pointcloud["pt_z"]]).T

""" 
Preprocess the pointcloud to isolate the tube 
This part will need to be modified depending on how the tube is presented in the pointcloud
If this part is modified, the code in the preprocess_pointcloud function will also need to be modified
"""

print("Preprocessing")
# Remove the lidar trajectory points
max_z = np.max(pointcloud[:, 2])
pointcloud = pointcloud[np.where(pointcloud[:, 2] < max_z - 0.3)]

###################### Remove the ground ###########################
plane = pyrsc.Plane()
_, plane_inliers = plane.fit(pointcloud, 0.2, maxIteration=500)

plane_mask = np.ones(len(pointcloud), bool)
plane_mask[plane_inliers] = 0
pointcloud = pointcloud[plane_mask]
#####################################################################


## The tube was located on two holders, so we need to remove them ###
min_x = min(pointcloud[:, 0])
max_x = max(pointcloud[:, 0])
pointcloud = pointcloud[np.where(pointcloud[:, 0] > min_x + 1.5)]
pointcloud = pointcloud[np.where(pointcloud[:, 0] < max_x - 1.5)]
#####################################################################

####################### Denoising with SOR ##########################
pointcloud_o3d = o3d.geometry.PointCloud()
pointcloud_o3d.points = o3d.utility.Vector3dVector(pointcloud)

_, noise_mask = pointcloud_o3d.remove_statistical_outlier(
    nb_neighbors=200, std_ratio=0.2
)
pointcloud = pointcloud[noise_mask]
view([pointcloud.T])
#####################################################################


#####################################################################
# We keep the index of each points of the tube so we don't have to
# recompute them later. They are in the plane_mask and the noise_mask
#####################################################################

arguments = {
    "lidar_data": lidar_data,
    "h5_info": h5_info,
    "plane_mask": plane_mask,
    "noise_mask": noise_mask,
    "measuring_head_180": args.measuring_head_180,
    "reversed": args.reversed
}

optimParameters = optim.minimize(
    fun=cost_funct,
    x0=initParameters,
    args=arguments,
    method="Nelder-Mead",
    # bounds= optim.Bounds([-1,-1,-2],[1,1,0]) #Setup translation parameters bounds
)

parameters = np.array(
    [
        optimParameters.x[0],
        optimParameters.x[1],
        optimParameters.x[2],
    ]
)

np.savetxt(os.path.join(args.output, "parameters.txt"), parameters)
print(parameters)
