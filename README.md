# Transformation matrix retriever

This program retrieves the rotation center of the measuring head of the Phenomobile.  
It uses a pointcloud of a long tube acquired by the Phenomobile to retrieve it.  


## Installation

Install the packages located in requirements.txt:  
`pip install -r requirements.txt`  
Install the CSF package from here: https://github.com/jianboqi/CSF


## Usage

Command line argument are the following:


| Name | Description |
| ------ | ------ |
| h5_path | The path to the HDF5 file. |
| output | The path to the folder where the transformation matrix will be outputted. |
| reversed | If the side lidar (lidar 3) is reversed. |
| measuring_head_180 | If the measuring head was rotated by 180° during the acquisition. |

Example command:

`python computecenterpos.py --h5_path path --output output --reversed`  
