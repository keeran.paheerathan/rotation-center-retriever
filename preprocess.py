import numpy as np
import open3d as o3d
import pyransac3d as pyrsc
import CSF

from pointcloudhandler import view

def remove_ground(pointcloud):
    print("Removing ground")
    csf = CSF.CSF()

    # prameter settings
    csf.params.bSloopSmooth = False
    csf.params.cloth_resolution = 0.1
    csf.params.rigidness = 3

    csf.params.time_step = 0.35
    csf.params.class_threshold = 0.1
    # more details about parameter: http://ramm.bnu.edu.cn/projects/CSF/download/

    csf.setPointCloud(pointcloud)
    ground = (
        CSF.VecInt()
    )  # a list to indicate the index of ground points after calculation
    non_ground = (
        CSF.VecInt()
    )  # a list to indicate the index of non-ground points after calculation
    csf.do_filtering(ground, non_ground)  # do actual filtering.

    return pointcloud[non_ground]

def preprocess_pointcloud(pointcloud, plane_mask, noise_mask):
    print("Processing pointcloud")

    """ Remove distant points """
    max_z = np.max(pointcloud[:, 2])
    pointcloud = pointcloud[np.where(pointcloud[:, 2] < max_z - 0.3)]

    try:
        pointcloud = pointcloud[plane_mask]
    except IndexError as e:
        print(e)
        view([pointcloud.T])

    min_x = min(pointcloud[:,0])
    max_x = max(pointcloud[:,0])
    pointcloud = pointcloud[np.where(pointcloud[:,0] > min_x + 1.5)]
    pointcloud = pointcloud[np.where(pointcloud[:,0] < max_x - 1.5)]
    
    try:
        pointcloud = pointcloud[noise_mask]
    except IndexError as e:
        pointcloud_o3d = o3d.geometry.PointCloud()
        pointcloud_o3d.points = o3d.utility.Vector3dVector(pointcloud)
        _, noise_mask = pointcloud_o3d.remove_statistical_outlier(
            nb_neighbors=200, std_ratio=0.2
        )
        pointcloud = pointcloud[noise_mask]
    
    return pointcloud